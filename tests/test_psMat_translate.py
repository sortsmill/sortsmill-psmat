#! /bin/env python
#
# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill PsMat.
# 
# Sorts Mill PsMat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill PsMat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import psMat
import random
import sys

eps = sys.float_info.epsilon

def test_translate (x, y):
    a = psMat.translate (x, y)
    print (x, y, a)

    if not isinstance (a, tuple):
        exit (10)
    if len (a) != 6:
        exit (20)
    for element in a:
        if not isinstance (element, float):
            exit (30)

    for i in range (0, 6):
        if 1000 * eps < abs (a[i] - (1, 0, 0, 1, x, y)[i]):
            exit (50)

random.seed ()

for i in range (0, 1000):
    test_translate (random.uniform (-1000, 1000),
                    random.uniform (-1000, 1000))

for i in range (0, 1000):
    test_translate (random.randrange (-1000, 1001),
                    random.randrange (-1000, 1001))

exit (0)

