# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill PsMat.
# 
# Sorts Mill PsMat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill PsMat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

$(eval include library.mk)

EXTRA_DIST =
DISTCLEANFILES = Makefile GNUmakefile library.mk atconfig
MOSTLYCLEANFILES =

# The `:;' works around a Bash 3.2 bug when the output is not writable.
$(srcdir)/package.m4: $(top_srcdir)/configure.ac
	:;{ \
		echo '# Signature of the current package.' && \
		echo 'm4_define([AT_PACKAGE_NAME],' && \
		echo '  [$(PACKAGE_NAME)])' && \
		echo 'm4_define([AT_PACKAGE_TARNAME],' && \
		echo '  [$(PACKAGE_TARNAME)])' && \
		echo 'm4_define([AT_PACKAGE_VERSION],' && \
		echo '  [$(PACKAGE_VERSION)])' && \
		echo 'm4_define([AT_PACKAGE_STRING],' && \
		echo '  [$(PACKAGE_STRING)])' && \
		echo 'm4_define([AT_PACKAGE_BUGREPORT],' && \
		echo '  [$(PACKAGE_BUGREPORT)])'; \
		echo 'm4_define([AT_PACKAGE_URL],' && \
		echo '  [$(PACKAGE_URL)])'; \
	} >'$(srcdir)/package.m4'

EXTRA_DIST += testsuite.at installed-testsuite.at
EXTRA_DIST += $(TESTSUITE) $(INSTALLED_TESTSUITE)
EXTRA_DIST += $(srcdir)/package.m4 atlocal.in
TESTSUITE = $(srcdir)/testsuite
INSTALLED_TESTSUITE = $(srcdir)/installed-testsuite

check-local: atconfig atlocal $(TESTSUITE)
	$(SHELL) '$(TESTSUITE)' $(TESTSUITEFLAGS) \
		PYTHONPATH=$(abs_top_builddir)/.libs 

installcheck-local: atconfig atlocal $(TESTSUITE)
	$(SHELL) '$(INSTALLED_TESTSUITE)' AUTOTEST_PATH='$(bindir)' $(TESTSUITEFLAGS)

clean-local:
	test ! -f '$(TESTSUITE)' || $(SHELL) '$(TESTSUITE)' --clean
	test ! -f '$(INSTALLED_TESTSUITE)' || $(SHELL) '$(INSTALLED_TESTSUITE)' --clean

AUTOM4TE = $(SHELL) $(top_srcdir)/build-aux/missing --run autom4te
AUTOTEST = $(AUTOM4TE) --language=autotest
$(TESTSUITE): $(srcdir)/testsuite.at $(srcdir)/package.m4 $(MY_AT_FILES)
	$(AUTOTEST) -I '$(srcdir)' -o $@.tmp $@.at
	mv $@.tmp $@
$(INSTALLED_TESTSUITE): $(srcdir)/installed-testsuite.at $(srcdir)/package.m4 $(MY_AT_FILES)
	$(AUTOTEST) -I '$(srcdir)' -o $@.tmp $@.at
	mv $@.tmp $@

EXTRA_DIST += test_psMat_identity.py
EXTRA_DIST += test_psMat_inverse.py
EXTRA_DIST += test_psMat_compose.py
EXTRA_DIST += test_psMat_rotate.py
EXTRA_DIST += test_psMat_scale.py
EXTRA_DIST += test_psMat_skew.py
EXTRA_DIST += test_psMat_translate.py

-include $(top_srcdir)/git.mk
