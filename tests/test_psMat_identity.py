#! /bin/env python
#
# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill PsMat.
# 
# Sorts Mill PsMat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill PsMat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import psMat
import sys

eps = sys.float_info.epsilon

a = psMat.identity ()
print (a)

if not isinstance (a, tuple):
    exit (10)
if len (a) != 6:
    exit (20)
for element in a:
    if not isinstance (element, float):
        exit (30)
for i in range (0, 6):
    if eps < abs (a[i] - (1, 0, 0, 1, 0, 0)[i]):
        exit (40)

exit (0)

