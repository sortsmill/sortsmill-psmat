# -*- coding: utf-8 -*-

# Copyright (C) 2012, 2013, 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill PsMat.
# 
# Sorts Mill PsMat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill PsMat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

# The copyright notice for the original PsMat module written in C,
# upon which Sorts Mill PsMat is based:
#
# Copyright (C) 2007-2012 by George Williams
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# The name of the author may not be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Transformations on PostScript matrices.

The matrices are expressed as six-element tuples of floats.

"""

cdef extern from "<math.h>" nogil:
  bint isfinite (long double)
  long double fabsl (long double)
  double cos (double)
  double sin (double)
  double tan (double)

class psMatException (Exception):
  def __init__ (self, irritants):
    self.irritants = irritants
  def __str__ (self):
    return str (self.irritants)

cdef psMat_invert (a):
  # Invert a PostScript matrix. Use Gauss-Jordan elimination with
  # partial pivoting. Use long double; we are not in a hurry. (If we
  # were, we would not be using Python.)

  cdef long double a0 = a[0]
  cdef long double a1 = a[1]
  cdef long double a2 = a[2]
  cdef long double a3 = a[3]
  cdef long double a4 = a[4]
  cdef long double a5 = a[5]

  cdef long double x11
  cdef long double x12
  cdef long double x13
  cdef long double x14
  cdef long double x21
  cdef long double x22
  cdef long double x23
  cdef long double x24

  #
  # Put the better choice of pivot in the first row.
  #
  if fabsl (a2) <= fabsl (a0):
    #
    # Augmented matrix X = x11 x12 x13 x14 = a[0] a[1]  1  0
    #                      x21 x22 x23 x24   a[2] a[3]  0  1
    #
    x11 = a0
    x12 = a1
    x13 = 1
    x14 = 0
    x21 = a2
    x22 = a3
    x23 = 0
    x24 = 1
  else:
    #
    # Augmented matrix X = x11 x12 x13 x14 = a[2] a[3]  0  1
    #                      x21 x22 x23 x24   a[0] a[1]  1  0
    #
    x11 = a2
    x12 = a3
    x13 = 0
    x14 = 1
    x21 = a0
    x22 = a1
    x23 = 1
    x24 = 0

  if x11 == 0:
    # The first pivot is zero.
    raise psMatException ([a, "The matrix is singular."])

  # Normalize row 1. Do not bother to set x11 ← 1.
  x14 /= x11
  x13 /= x11
  x12 /= x11

  # Scale row 1 by x21 and subtract it from row 2. Do not bother to
  # set x21 ← 0.
  x24 -= x21 * x14
  x23 -= x21 * x13
  x22 -= x21 * x12

  if x22 == 0:
    # The second pivot is zero.
    raise psMatException ([a, "The matrix is singular or nearly singular."])

  # Normalize row 2. Do not bother to set x22 ← 1.
  x24 /= x22
  x23 /= x22

  # Scale row 2 by x12 and subtract it from row 1. Do not bother to
  # set x12 ← 0.
  x14 -= x12 * x24
  x13 -= x12 * x23

  #
  # The inverse = x13  x14
  #               x23  x24
  #

  # Compute the offset in the plane.
  cdef long double b1 = -(a4 * x13) - (a5 * x23)
  cdef long double b2 = -(a4 * x14) - (a5 * x24)

  result = (x13, x14, x23, x24, b1, b2)

  if not (isfinite(x13) and
          isfinite(x14) and
          isfinite(x23) and
          isfinite(x24) and
          isfinite(b1) and
          isfinite(b2)):
    raise psMatException ([a, "The result contains non-finite floating point numbers.",
                           result])

  return result

cdef compose_two_matrices (mat1, mat2):
  cdef double a0 = mat1[0]
  cdef double a1 = mat1[1]
  cdef double a2 = mat1[2]
  cdef double a3 = mat1[3]
  cdef double a4 = mat1[4]
  cdef double a5 = mat1[5]

  cdef double b0 = mat2[0]
  cdef double b1 = mat2[1]
  cdef double b2 = mat2[2]
  cdef double b3 = mat2[3]
  cdef double b4 = mat2[4]
  cdef double b5 = mat2[5]

  cdef double c0 = (a0 * b0) + (a1 * b2)
  cdef double c1 = (a0 * b1) + (a1 * b3)
  cdef double c2 = (a2 * b0) + (a3 * b2)
  cdef double c3 = (a2 * b1) + (a3 * b3)
  cdef double c4 = (a4 * b0) + (a5 * b2) + b4;
  cdef double c5 = (a4 * b1) + (a5 * b3) + b5;

  return (c0, c1, c2, c3, c4, c5)

def identity ():
  """Return an identity matrix as a six-element tuple."""
  return (1.0, 0.0, 0.0, 1.0, 0.0, 0.0)

def compose (mat1, mat2 = None, *rest):
  """Return a matrix that is the composition of one or
  more input transformations.
    
  """
  if mat2 is None:
    assert (len (rest) == 0)
    result = mat1
  else:
    result = compose_two_matrices (mat1, mat2)
    for m in rest:
      result = compose_two_matrices (result, m)
  return result

def inverse (mat):
  """Return a matrix which is the inverse of the input
  transformation.

  """
  return psMat_invert (mat)

def rotate (theta):
  """Return a matrix which will rotate by an angle expressed
  in radians.

  """
  cdef double t = theta
  cdef double cosine = cos(t)
  cdef double sine = sin(t)
  return (cosine, sine, -sine, cosine, 0.0, 0.0)

def scale (x, y = None):
  """Return a matrix that will scale."""
  if y is None:
    y = x
  return (float(x), 0.0, 0.0, float(y), 0.0, 0.0)

def skew (theta):
  """Return a matrix that will skew."""
  cdef double t = theta
  cdef double tangent = tan(t)
  return (1.0, 0.0, tangent, 1.0, 0.0, 0.0)

def translate (x, y):
  """Return a matrix that will translate."""
  return (1.0, 0.0, 0.0, 1.0, float(x), float(y))
